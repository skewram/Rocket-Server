# Rocket Server

Serveur utilisé par la fusée du projet **SpaceH2O** visant à la faire communiquer avec le sol et assurer la transmission des données et l'intégrité de la fusée ainsi que de ses fonctions.

|  Spécification  |  Status  |
| :------------: | :------------: |
|  Qualité du  code  |  [![Codacy Badge](https://api.codacy.com/project/badge/Coverage/8bbd1e80a0ed4b95ad215f23ac08e717)](https://www.codacy.com/manual/noan.perrot/Rocket-Server?utm_source=github.com&utm_medium=referral&utm_content=Skeeww/Rocket-Server&utm_campaign=Badge_Coverage)  |

### Fonctionalitées Disponibles

-  Vérification de l'ensemble des sytèmes et sous-systèmes
-  Paramétrages de différents capteurs
-  Lancement des processus sous forme de Threads

### Dépendances

-  [Pusher](http://pusher.com "Pusher")
-  [MongoDB Atlas](https://www.mongodb.com/cloud/atlas "MongoDB Atlas")
