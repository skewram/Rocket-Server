import asyncio, integrity, colored

async def RunIntegrity():
    if integrity.checkNetwork():
        print(colored.stylize("[OK]", colored.fg("green")) + " Network Connection")
    else:
        print(colored.stylize("[ERROR]", colored.fg("red")) + " Network Connection")
    if integrity.checkPusher():
        print(colored.stylize("[OK]", colored.fg("green")) + " Pusher Connection")
    else:
        print(colored.stylize("[ERROR]", colored.fg("red")) + " Pusher Connection")
    if integrity.checkMongo():
        print(colored.stylize("[OK]", colored.fg("green")) + " MongoDB Connection")
    else:
        print(colored.stylize("[ERROR]", colored.fg("red")) + " MongoDB Connection")
    if integrity.checkProcessor():
        print(colored.stylize("[OK]", colored.fg("green")) + " Processor is under 80%")
    else:
        print(colored.stylize("[WARNING]", colored.fg("orange_4a")) + " Processor is upper 80%")
    if integrity.checkRam():
        print(colored.stylize("[OK]", colored.fg("green")) + " Ram is under 80%")
    else:
        print(colored.stylize("[WARNING]", colored.fg("orange_4a")) + " Ram is upper 80%")

async def TestThread():
    print("Hey I'm the test thread !")
    await asyncio.sleep(1)

async def runThreads():
    Thread_RunIntegrity = asyncio.create_task(RunIntegrity())
    await Thread_RunIntegrity
    Thread_TestThread = asyncio.create_task(TestThread())
    await Thread_TestThread

if __name__ == "__main__":
    asyncio.run(runThreads())
