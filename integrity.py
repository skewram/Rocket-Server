import socket, pusher, psutil

def checkProcessor():
    if psutil.cpu_percent(1, False) <= 80:
        return True
    else:
        return False

def checkRam():
    if psutil.virtual_memory().percent <= 80:
        return True
    else:
        return False

def checkNetwork():
    try:
        host = socket.gethostbyname("www.google.fr")
        connection = socket.create_connection((host, 80), 5)
        connection.close()
        return True
    except:
        return False

def checkPusher():
    try:
        client = pusher.Pusher(
            app_id='862851',
            key='PUSHER_KEY',
            secret='PUSHER_SECRET',
            cluster='eu',
            ssl=True
        )
        if len(client.channels_info()):
            return True
    except:
        pass
    return False

def checkMongo():
    return True
